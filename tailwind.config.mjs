/** @type {import('tailwindcss').Config} */
export default {
  content: ["./src/**/*.{astro,html,js,jsx,md,mdx,svelte,ts,tsx,vue}"],
  theme: {
    extend: {
      fontFamily: {
        sc: ["'Alegreya SC'", "serif"],
        sans: ["'Alegreya Sans'", "sans-serif"],
        serif: ["Alegreya", "serif"],
      },
      fontSize: {
        xs: "0.625rem",
        sm: "0.812rem",
        base: "1rem",
        xl: "1.25rem",
        "2xl": "1.562rem",
        "3xl": "1.938rem",
        "4xl": "2.438rem",
        "5xl": "3.062rem",
        "6xl": "3.812rem",
      },
      screens: {
        tablet: "834px", // ipad Pro 11
        laptop: "1280px", // MacBook Pro
        desktop: "1920px",
        widescreen: "2880px",
      },
      colors: {
        blue: {
          light: "#ECF2F9",
          DEFAULT: "#143352",
          dark: "#0F263D",
          darker: "#0A1929",
        },
        yellow: "#F5E6BC",
      },
    },
  },
  plugins: [require("@tailwindcss/typography")],
};
